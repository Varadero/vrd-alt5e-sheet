import { DND5E } from '/systems/dnd5e/module/config.js';
import Actor5e from '/systems/dnd5e/module/actor/entity.js';
import ActorSheet5eVehicle from '/systems/dnd5e/module/actor/sheets/vehicle.js';

// All the lovely skills, proficiencies, types, and properties
// that make Dark Matter Special.
DND5E.skills['dat'] = 'Data';
DND5E.skills['pil'] = 'Piloting';
DND5E.skills['tec'] = 'Technology';
DND5E.weaponProficiencies['simpleB'] = 'Simple Blasters';
DND5E.weaponProficienciesMap['simpleB'] = 'simpleB';
DND5E.weaponProficiencies['martialB'] = 'Martial Blasters';
DND5E.weaponProficienciesMap['martialB'] = 'martialB';
DND5E.weaponTypes['simpleB'] = 'Simple Blasters';
DND5E.weaponTypes['martialB'] = 'Martial Blasters';
DND5E.weaponProperties['aut'] = 'Automatic';
DND5E.weaponProperties['blas'] = 'Blaster';
DND5E.weaponProperties['explo'] = 'Explosive';
DND5E.weaponProperties['fist'] = 'Fist';
DND5E.weaponProperties['fore'] = 'Foregrip';
DND5E.weaponProperties['heat'] = 'Heat';
DND5E.weaponProperties['mount'] = 'Mounted';
DND5E.weaponProperties['nonle'] = 'Nonlethal';
DND5E.weaponProperties['oheat'] = 'Overheat';
DND5E.weaponProperties['scat'] = 'Scatter';
DND5E.weaponProperties['sight'] = 'Sighted';


Hooks.on('init', async function() {
  console.debug('DME | This code runs once the Foundry VTT software begins'+
    ' it\'s initialization workflow.');
  $('body').addClass('dme');
  libWrapper.register('dme', 'CONFIG.Actor.documentClass.prototype.prepareData',
      function(wrapped) {
        console.debug('DME | Adding Dark Matter skills');
        const skills = this.data._source.data.skills;
        const type = this.data.type;
        if (type != 'vehicle') {
          skills['dat'] = { value: 0, ability: 'int', ...skills['dat'] };
          skills['pil'] = { value: 0, ability: 'dex', ...skills['pil'] };
          skills['tec'] = { value: 0, ability: 'int', ...skills['tec'] };
        }
        return wrapped(this);
      }
      , 'WRAPPER');
  libWrapper.register('dme', 'CONFIG.Item.documentClass.prototype.abilityMod',
      function(wrapped) {
        let result = wrapped();
        const itemData = this.data.data;
        const wt = itemData.weaponType;
        if (['simpleB', 'martialB'].includes(wt)) {
          if (itemData.ability) {
            return itemData.ability;
          }
          return 'dex';
        }
        return result;
      }, 'MIXED');
});

export class DarkMatterShipSheet extends ActorSheet5eVehicle {
  get template() {
    if ( !game.user.isGM && this.actor.limited ) {
      return 'systems/dnd5e/templates/actors/limited-sheet.html';
    }
    return `modules/dme/templates/${this.actor.data.type}-sheet.html`;
  }
}


Hooks.on('ready', function() {
  console.debug('DME | This code runs once core initialization is ready and'+
    ' game data is available.');
  if(!game.modules.get('lib-wrapper')?.active && game.user.isGM) {
    ui.notifications.error('Module DME(Dark Matter Extensions) requires'+
      ' the "libWrapper" module. Please install and activate it.');
  }
});
